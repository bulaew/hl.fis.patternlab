module.exports = {};

// basic reactJS
import React from 'react';
import ReactDOM from 'react-dom';

import { Router, browserHistory } from 'react-router';
// browser-history & routing
import { syncHistoryWithStore } from 'react-router-redux';

// connect react with redux -> app-states
import { Provider } from 'react-redux';

// import to create an initial app-store
import initStore from 'create';

// insert Redux Devtools -> not in production -> Shows app states
import DevTools from './components/container/DevTools';


import * as fisui from '../../fis-ui-components/components/index.js';
Object.assign(module.exports, fisui);

import getRoutes from './routes';

// create initial stores
const store = initStore();

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store} key="provider">
        <div className="appContainer">
            <Router history={history}>{getRoutes(store)}</Router>
            <DevTools />
        </div>
    </Provider>,
    document.getElementById('app')
);
