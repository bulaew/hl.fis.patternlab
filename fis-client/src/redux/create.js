import { createStore, compose, applyMiddleware } from 'redux';
import reducer from './reducers/_index';

// middleware to use with AJAX-calls, f.e.
import promiseMiddleware from 'redux-promise-middleware';

const enhancer = compose(
    // Required! Enable Redux DevTools with the monitors you chose
    applyMiddleware(promiseMiddleware({
        promiseTypeSuffixes: ['LOADING', 'SUCCESS', 'ERROR']
    }))
);

export default function initStore() {
    const store = createStore(reducer, enhancer);
    return store;
}

