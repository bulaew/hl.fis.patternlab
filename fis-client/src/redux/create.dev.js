import { createStore, compose, applyMiddleware } from 'redux';
import reducer from './reducers/_index';
import { browserHistory } from 'react-router';

import DevTools from '../components/container/DevTools';

// middleware to use with AJAX-calls, f.e.
import promiseMiddleware from 'redux-promise-middleware';

import { routerMiddleware } from 'react-router-redux';

const enhancer = compose(
    applyMiddleware(promiseMiddleware({
        promiseTypeSuffixes: ['LOADING', 'SUCCESS', 'ERROR']
    })),
    applyMiddleware(routerMiddleware(browserHistory)),
    // Required! Enable Redux DevTools with the monitors you chose
    DevTools.instrument()
);

export default function initStore() {
    const store = createStore(reducer, enhancer);
    return store;
}
