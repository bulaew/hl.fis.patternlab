import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

// in alphabetic order please
import globalerrors from './globalerrors';
import globalloading from './globalloading';
import openAppsList from './openApps';
import user from './user';

// in alphabetic order please
export default combineReducers({
    globalerrors,
    globalloading,
    openAppsList,
    routing: routerReducer,
    user
});
