export default function setOpenNewApp(app) {
    // console.log('action openNewApps was called', app);
    return {
        type: 'OPEN_NEW_APP',
        payload: {
            app
        }
    };
}
