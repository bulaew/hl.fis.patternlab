export default function setCurrentApp(app) {
    // console.log('action SET_CURRENT_APP was called', app);
    return {
        type: 'SET_CURRENT_APP',
        payload: {
            app
        }
    };
}
