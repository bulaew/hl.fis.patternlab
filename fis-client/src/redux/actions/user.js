import request from 'superagent-es6-promise';

export default function loginUser(user) {
    const pendingRequest = request
        .post('/fis/api/auth/login')
        // .post('/login')
        .send({ username: user.userID, password: user.userPassword, organisation: user.userOrganisation })
        .set('Accept', 'application/json')
        .promise();
    return {
        type: 'LOGIN_USER',
        payload: {
            promise: pendingRequest
        }
    };
}
