import React from 'react';

export default class FisApplicationContainer extends React.Component {
    static propTypes = {
        params: React.PropTypes.any,
        route: React.PropTypes.any
    };

    static contextTypes = {
        router: React.PropTypes.object
    };

    state = {
        appElement: null
    };

    componentDidMount = () => {
        console.log('MOUNTED: ' + this.props.params.trancode);

        const script = document.createElement('script');
        script.src = '../' + this.props.params.trancode + '.js';
        script.type = 'text/javascript';
        script.async = 1;

        console.log(script.src);

        script.onload = () => {
            console.log('SCRIPT LOADED');
            /* eslint-disable */
            // ToDo replace renderApp() with a specific renderApp()
            this.setState({appElement: renderApp()});
            console.log('DONE');
            /* eslint-enable */
        };

        script.onreadystatechange = () => {
            if (this.readyState === 'complete' || this.readyState === 'loaded') {
                script.onload();
            }
        };

        script.onerror = (event) => {
            console.log('SCRIPT HAD ERRORS');
            console.log(event);
        };

        document.body.appendChild(script);

        this.context.router.setRouteLeaveHook(this.props.route, this.routerWillLeave);
    };

    routerWillLeave = (nextLocation) => {
        console.log('FisApplicationContainer routerWillLeave:', nextLocation);
        let message = '';
        // return false to prevent a transition w/o prompting the user,
        // or return a string to allow the user to decide:
        if (!this.state.isSaved) {
            message = 'Your work is not saved! Are you sure you want to leave?';
        } else {
            message = false;
        }
        return message;
    };


    render() {
        // const script = 'apps/' + this.props.params.trancode + '.js';
        let result;
        if (this.state.appElement) {
            result = this.state.appElement;
        } else {
            result = (<p>Loading app {this.props.params.transcode}</p>);
        }
        return result;
    }
}
