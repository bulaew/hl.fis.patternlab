const path = require('path');
const named = require('vinyl-named');
const webpack = require('webpack-stream');


module.exports = (context) => {
    return () => {
        const production = process.env.NODE_ENV === 'production';

        const WEBPACK_OPTIONS = {
            output: {
                filename: 'fisclient.js',
                library: 'Fis',
                libraryTarget: 'umd',
                umdNamedDefine: true
            },

            resolve: {
                root: [
                    path.resolve(`${context.fisclient.src}/src`),
                    path.resolve(`${context.fisuicomponents.src}/components`),
                    path.resolve(`${context.fisuicomponents.src}/components/00-globals`),
                    path.resolve(`${context.fisuicomponents.src}/components/01-atoms`),
                    path.resolve(`${context.fisuicomponents.src}/components/02-molecules`),
                    path.resolve(`${context.fisuicomponents.src}/components/03-organisms`),
                    path.resolve(`${context.fisuicomponents.src}/components/04-business`)
                ],
                extensions: ['', '.js', '.jsx'],
                alias: {
                    'create': production ? './redux/create' : './redux/create.dev'
                }
            },
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        loaders: ['babel'],
                        include: [
                            path.resolve(context.fisclient.src),
                            path.resolve(path.join(context.fisuicomponents.src, 'components'))
                        ]
                    },
                    { test: require.resolve('react'), loader: 'expose?React' },
                    { test: require.resolve('react-dom'), loader: 'expose?ReactDOM' }
                ]
            },
            devtool: 'cheap-module-source-map'
        };

        context.gulp.src(`${context.fisclient.src}/src/index.js`)
            .pipe(named())
            .pipe(webpack(WEBPACK_OPTIONS))
            .on('error', context.gulpNotify.onError('<%= error.message %>'))
            .pipe(context.gulp.dest(context.fisclient.target))
            .pipe(context.gulpPlugins.cached('fis:client:js:build'))
            .pipe(context.gulpPlugins.if(context.browserSync.active, context.browserSync.reload({ stream: true })));
    };
};
