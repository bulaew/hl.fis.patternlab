import React, { Component } from 'react';
import FisInputBase from 'fis-input-base';

export default class FisInputEquipmentNumber extends Component {
    static defaultProps = {
        colSpan: 8
    };

    render() {
        return (<FisInputBase {...this.props} colSpan={8} />);
    }
}
