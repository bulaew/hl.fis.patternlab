import React, { Component } from 'react';
import classnames from 'classnames';

export default class FisIcon extends Component {

    static propTypes = {
        variant: React.PropTypes.oneOf(['default', 'sizeM', 'sizeL', 'sizeXL']),
        icon: React.PropTypes.string,
        className: React.PropTypes.string,
        children: React.PropTypes.string
    };

    static defaultProps = {
        variant: 'default'
    };


    render() {
        const classes = classnames('fis-icon', this.props.variant, this.props.icon, this.props.className);
        return (
            <i className={classes}>
                <span className="show-for-sr" aria-label={this.props.children || 'Button'}>
                  {this.props.children || 'Button'}
                </span>
            </i>
        );
    }
}
