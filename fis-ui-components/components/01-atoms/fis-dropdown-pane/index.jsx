import React, { Component } from 'react';

export default class FisDropDownPane extends Component {

    static propTypes = {
        id: React.PropTypes.string,
        className: React.PropTypes.string,
        title: React.PropTypes.string,
        children: React.PropTypes.arrayOf(React.PropTypes.element)
    };

    render() {
        const align = 'bottom';
        const id = this.props.id;
        const classes = ['dropdown-pane', align, this.props.className].join(' ').trim();
        const title = this.props.title;

        return (
            <div is>
                <button data-toggle={id}>{title}</button>
                <div className={classes} data-dropdown data-hover="true" id={id}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
