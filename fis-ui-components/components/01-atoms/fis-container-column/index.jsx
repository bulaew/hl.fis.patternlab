import React, { Component } from 'react';
import classnames from 'classnames';

export default class FisContainerColumn extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        colSpan: React.PropTypes.number,
        children: React.PropTypes.node
    };

    render() {
        const classes = classnames('fis-grid-column', this.props.className, {
            [`fis-grid-colSpans-${this.props.colSpan}`]: this.props.colSpan
        });
        return (
            <div className={classes}>
                {React.Children.map(this.props.children, (child) => {
                    return child;
                })}
            </div>
        );
    }
}
