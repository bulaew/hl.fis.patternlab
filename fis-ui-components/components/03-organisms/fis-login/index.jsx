import React, { Component } from 'react';
// import classnames from 'classnames';
import FisSection from 'fis-section';
import FisInputGroup from 'fis-input-group';
import FisContainerRow from 'fis-container-row';
import FisContainerColumn from 'fis-container-column';
import FisFieldset from 'fis-fieldset';
import FisInputBase from 'fis-input-base';
import FisButtonGroup from 'fis-button-group';
import FisButton from 'fis-button';
import FisText from 'fis-text';

export default class FisLogin extends Component {
    static propTypes = {
        loginUser: React.PropTypes.func,
        loginWithToken: React.PropTypes.bool,
        loading: React.PropTypes.bool,
        error: React.PropTypes.object
    }

    static defaultprops ={
        loginWithToken: false,
        loading: false
    };

    state = {
        isOpen: 'login',
        loginData: {}
    };

    openChangePW = (event) => {
        event.preventDefault();
        console.log('user logged in to change password... maybe');
        // if user i logged in to cgange PW, then change view
        this.setState({
            isOpen: 'changePW'
        });
    };

    login = (event) => {
        event.preventDefault();
        if (this.props.loginWithToken) {
            console.log('user logged in... maybe');
            // if user i logged in, then change view
            this.setState({
                isOpen: 'tokenVerification'
            });
        } else {
            this.props.loginUser(this.state.loginData);
        }
    };

    loginWithToken = (event) => {
        event.preventDefault();
        console.log('user logged in... REALLY!');
        this.props.loginUser(this.state.loginData);
        // TODO: reall login
    };

    updateLoginData = (name, value) => {
        const loginData = this.state.loginData;
        loginData[name] = value;
        this.setState({ loginData });
    };

    render() {
        const loginBtnTxt = this.props.loading ? 'Loading...' : 'Login';

        return (
            <FisSection className="fis-login">
                <form>
                    <FisContainerRow>
                        <FisContainerColumn >

                            <span display-if={this.props.error}>{this.props.error.message}</span>
                            <FisFieldset legend="Login" display-if={this.state.isOpen === 'login'}>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisInputGroup label="User ID">
                                            <FisInputBase name="userID" handleChange={this.updateLoginData} />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisInputGroup label="Password" >
                                            <FisInputBase name="userPassword" type="password" handleChange={this.updateLoginData} />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisInputGroup label="Organisation">
                                            <FisInputBase name="userOrganisation" handleChange={this.updateLoginData} />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisButtonGroup>
                                            <FisButton variant="content-default" onClick={this.openChangePW}>Change Password</FisButton>
                                            <FisButton variant="content-default" onClick={this.toggleFisCollapsed}>Cancel</FisButton>
                                            <FisButton variant="content-hero" onClick={this.login}>{loginBtnTxt}</FisButton>
                                        </FisButtonGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                            </FisFieldset>

                            <FisFieldset legend="Change Password" display-if={this.state.isOpen === 'changePW'}>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisInputGroup label="New Password">
                                            <FisInputBase />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisInputGroup label="Verify new Password">
                                            <FisInputBase type="password" />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisButtonGroup>
                                            <FisButton variant="content-default" onClick={this.toggleFisCollapsed}>Cancel</FisButton>
                                            <FisButton variant="content-hero" onClick={this.login}>Login</FisButton>
                                        </FisButtonGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                            </FisFieldset>

                            <FisFieldset legend="A dual authentication ist required." display-if={this.state.isOpen === 'tokenVerification'}>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisText>Please provide security-token by generator. Need help? Contact Helpdesk</FisText>
                                        <FisInputGroup label="Security token">
                                            <FisInputBase />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={16}>
                                        <FisButtonGroup>
                                            <FisButton variant="content-default" onClick={this.toggleFisCollapsed}>Cancel</FisButton>
                                            <FisButton variant="content-hero" onClick={this.loginWithToken}>Login</FisButton>
                                        </FisButtonGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                            </FisFieldset>
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </FisSection>
        );
    }
}
