import React, { Component } from 'react';
import AriaTabPanel from 'react-aria-tabpanel';

export default class FisTabPanel extends Component {

    static propTypes = {
        tabData: React.PropTypes.array
    };

    state = { activeTab: '1' };

    setTab = (newActiveTabId) => {
        this.setState({ activeTab: newActiveTabId });
    };

    render() {
        const { activeTab } = this.state;

        const tabs = this.props.tabData.map((t, i) => {
            let innerCl = 'tabs-tabInner';
            if (t.id === activeTab) innerCl += ' is-active';
            return (
                <li className="tabs-tablistItem" key={i}>
                    <AriaTabPanel.Tab tabId={t.id} className="tabs-tab">
                        <div className={innerCl}>
                            {t.title}
                        </div>
                    </AriaTabPanel.Tab>
                </li>
            );
        });

        const panels = this.props.tabData.map((p, i) => {
            return (
                <AriaTabPanel.TabPanel tabId={p.id} key={i}>
                    {p.content}
                </AriaTabPanel.TabPanel>
            );
        });

        return (
            <AriaTabPanel.Wrapper
                onChange={this.setTab}
                activeTabId="1"
                className="fis-tabpanel"
            >
                <AriaTabPanel.TabList>
                    <ul className="tabs-tablist">
                        {tabs}
                    </ul>
                </AriaTabPanel.TabList>
                <div className="tabs-panel">
                    {panels}
                </div>
            </AriaTabPanel.Wrapper>
        );
    }
}
