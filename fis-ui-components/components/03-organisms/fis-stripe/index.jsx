import React, { Component } from 'react';
import classnames from 'classnames';
import FisSection from 'fis-section';
import FisButtonIcon from 'fis-button-icon';
import FisButtonPanel from 'fis-button-panel';

export default class FisStripe extends Component {

    static propTypes = {
        title: React.PropTypes.string,
        isOpened: React.PropTypes.bool,
        children: React.PropTypes.oneOfType([React.PropTypes.arrayOf(React.PropTypes.node), React.PropTypes.node]),
        buttonGroups: React.PropTypes.array
    };

    state = {
        isOpened: this.props.isOpened
    };

    toggleContentHeight = () => {
        if (this.state.isOpened) {
            return 'isOpened';
        }
        return 'isClosed';
    };

    toggleFisCollapsed = () => {
        this.setState({
            isOpened: !this.state.isOpened
        });
    };

    render() {
        const sectionClass = classnames('fis-section-content', this.toggleContentHeight());
        return (
            <FisSection className="fis-stripe" >
                <header>
                    <h2>{this.props.title}</h2>
                    <FisButtonIcon
                        display-if={!this.state.isOpened}
                        icon="fis-icon-chevron-down"
                        onClick={this.toggleFisCollapsed}
                        tooltip="Toggle section"
                    />
                    <FisButtonIcon
                        display-if={this.state.isOpened}
                        icon="fis-icon-chevron-up"
                        onClick={this.toggleFisCollapsed}
                        tooltip="Toggle section"
                    />
                </header>
                <FisSection className={sectionClass} isOpened={this.props.isOpened}>
                    <div className="inner">
                        <div className="bottom-border" />
                        {this.props.children}
                    </div>
                    <footer>
                        <FisButtonPanel buttonGroups={this.props.buttonGroups} />
                    </footer>
                </FisSection>
            </FisSection>
        );
    }
}
