import React, { Component } from 'react';

import FisClientHeader from 'fis-client-header';

export default class FisClient extends Component {
    static propTypes = {
        children: React.PropTypes.oneOfType([React.PropTypes.arrayOf(React.PropTypes.element), React.PropTypes.element]),
        openAppList: React.PropTypes.object,
        applicationMenu: React.PropTypes.array,
        favouriteMenu: React.PropTypes.array,
        user: React.PropTypes.object,
        redirectAferterLogin: React.PropTypes.func,
        setOpenNewApp: React.PropTypes.func,
        setCurrentApp: React.PropTypes.func,
        setCloseApp: React.PropTypes.func
    };

    state = {
        modalIsOpen: false,
        modalContent: 'Ich bin ein Modal-Content'
    };

    componentWillReceiveProps(nextProps) {
        if (!this.props.user && nextProps.user) {
            // login
            this.props.redirectAferterLogin(this.props.openAppList.apps[0].app);
        } else if (this.props.user && !nextProps.user) {
            // logout
            //  this.props.redirect2Dashboard();
        }
    }

    openModal = () => {
        this.setState({
            modalIsOpen: true,
            modalContent: 'Ich bin ein Modal-Content'
        });
    };

    closeModal = () => {
        this.setState({
            modalIsOpen: false,
            modalContent: ''
        });
    };

    handleModalCloseRequest = () => {
        // opportunity to validate something and keep the modal open even if it
        // requested to be closed
        this.setState({
            modalIsOpen: false
        });
    };

    customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)'
        }
    };

    render() {
        return (
            <div className="fis-client" id="fis-client">
                <FisClientHeader
                    openAppList={this.props.openAppList}
                    applicationMenu={this.props.applicationMenu}
                    favouriteMenu={this.props.favouriteMenu}
                    user={this.props.user}
                    setOpenNewApp={this.props.setOpenNewApp}
                    setCurrentApp={this.props.setCurrentApp}
                    setCloseApp={this.props.setCloseApp}
                />
                <div className="fis-clientcontent">
                    {this.props.children}
                </div>
            </div>
        );
    }
}
