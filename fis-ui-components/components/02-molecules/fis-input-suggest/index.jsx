import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';

const result = [{
    name: 'C',
    year: 1972
}, {
    name: 'C#',
    year: 2000
}, {
    name: 'C++',
    year: 1983
}, {
    name: 'Clojure',
    year: 2007
}, {
    name: 'Elm',
    year: 2012
}, {
    name: 'Go',
    year: 2009
}, {
    name: 'Haskell',
    year: 1990
}, {
    name: 'Java',
    year: 1995
}, {
    name: 'Javascript',
    year: 1995
}, {
    name: 'Perl',
    year: 1987
}, {
    name: 'PHP',
    year: 1995
}, {
    name: 'Python',
    year: 1991
}, {
    name: 'Ruby',
    year: 1995
}, {
    name: 'Scala',
    year: 2003
}];

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim()); // See: https://github.com/moroshko/react-autosuggest/blob/master/demo/src/components/utils/utils.js#L2-L4

    if (escapedValue === '') {
        return [];
    }

    const regex = new RegExp('^' + escapedValue, 'i');

    return result.filter(language => { return regex.test(language.name);});
}

function getSuggestionValue(suggestion) { // when suggestion selected, this function tells
    return suggestion.name;                 // what should be the value of the input
}

function renderSuggestion(suggestion) {
    return (
        <span>{suggestion.name}</span>
    );
}

export default class FisInputSuggest extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        defaultValue: React.PropTypes.oneOfType(
            [
                React.PropTypes.string,
                React.PropTypes.number
            ]),
        disabled: React.PropTypes.bool,
        defaultChecked: React.PropTypes.bool,
        id: React.PropTypes.string,
        mandatory: React.PropTypes.bool,
        name: React.PropTypes.string,
        placeholder: React.PropTypes.string,
        value: React.PropTypes.string,
        readOnly: React.PropTypes.bool,
        type: React.PropTypes.string
    };

    state = {
        value: '',
        suggestions: getSuggestions('')
    };

    onChange= (event, { newValue }) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsUpdateRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    render() {
        const { suggestions } = this.state;
        const inputProps = {
            placeholder: this.props.placeholder,
            value: this.props.defaultValue,
            onChange: this.onChange,
            type: 'search',
            disabled: this.props.disabled
        };

        return (
            <Autosuggest suggestions={suggestions}
                onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputProps}
            />
        );
    }
}
