import React, { Component } from 'react';
import classnames from 'classnames';

import FisContainerRow from 'fis-container-row';
import FisContainerColumn from 'fis-container-column';
import FisButtonIcon from 'fis-button-icon';

export default class FisInputPickFlow extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        children: React.PropTypes.oneOfType([React.PropTypes.arrayOf(React.PropTypes.element), React.PropTypes.element]),
        focusError: React.PropTypes.func,
        hideError: React.PropTypes.func,
        mouseOverError: React.PropTypes.func,
        mouseOutError: React.PropTypes.func
    };

    extendChild(child) {
        return React.cloneElement(child, {
            className: classnames(this.props.className),
            focusError: this.props.focusError,
            hideError: this.props.hideError,
            mouseOverError: this.props.mouseOverError,
            mouseOutError: this.props.mouseOutError
        });
    }

    render() {
        const renderChildren = React.Children.map(this.props.children, (child) => {
            return (
                <FisContainerColumn className={child.props.className} colSpan={child.props.colSpan}>
                    {this.extendChild(child)}
                </FisContainerColumn>
            );
        });
        return (
            <div className="pick-flow">
                <FisContainerRow>
                    { renderChildren }
                </FisContainerRow>
                <FisButtonIcon className="input-group-button" icon="fis-icon-pickflow" tooltip="open Pick-Flow" />
            </div>
        );
    }
}
