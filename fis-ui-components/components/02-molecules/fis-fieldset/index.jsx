import React, { Component } from 'react';
import classnames from 'classnames';

import FisButtonIcon from 'fis-button-icon';

export default class FisFieldset extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        legend: React.PropTypes.string,
        legendClass: React.PropTypes.string,
        mandatory: React.PropTypes.bool,
        infoflow: React.PropTypes.bool,
        children: React.PropTypes.oneOfType([React.PropTypes.arrayOf(React.PropTypes.element), React.PropTypes.element])
    };

    render() {
        const classes = classnames('fis-fieldset', this.props.className, {
            'info-flow': !this.props.legendClass
        });

        const legendClasses = classnames(this.props.legendClass, {
            'no-legend': !this.props.legend
        });

        return (
            <fieldset className={classes}>
              <span>
                  <legend className={legendClasses}>
                      {this.props.legend}
                      <span display-if={this.props.mandatory} className="fis-icon fis-icon-asterisk"></span>
                      <FisButtonIcon display-if={this.props.infoflow} icon="fis-icon-infoflow" tooltip="open Info-Flow" />
                  </legend>
              </span>
                {this.props.children}
            </fieldset>
        );
    }
}
