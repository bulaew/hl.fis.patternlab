import React, { Component } from 'react';
import classnames from 'classnames';

import FisButtonIcon from 'fis-button-icon';

export default class FisLabel extends Component {

    static propTypes = {
        children: React.PropTypes.any,
        className: React.PropTypes.string,
        for: React.PropTypes.string,
        label: React.PropTypes.string,
        isInvalid: React.PropTypes.bool,
        infoflow: React.PropTypes.bool,
        labelRight: React.PropTypes.bool
    };

    render() {
        const opts = {};

        opts.className = classnames('fis-label', this.props.className, {
            'is-invalid-label': this.props.isInvalid
        });


        if (this.props.for) {
            opts.htmlFor = this.props.for;
        }

        return (
            <label {...opts}>
                <span display-if={this.props.labelRight}>
                    {this.props.children}
                    <span className="label-text">{this.props.label}</span>
                    <FisButtonIcon display-if={this.props.infoflow}icon="fis-icon-info-flow-default size-s">open Info-Flow</FisButtonIcon>
                </span>
                <span display-if={!this.props.labelRight}>
                    <span className="label-text">{this.props.label}</span>
                    <FisButtonIcon display-if={this.props.infoflow} icon="fis-icon-info-flow-default size-s">open Info-Flow</FisButtonIcon>
                    {this.props.children}
                </span>
            </label>
        );
    }
}
