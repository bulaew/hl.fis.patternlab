import React, { Component } from 'react';
import classnames from 'classnames';
import FisLabel from 'fis-label';
import FisContainerRow from 'fis-container-row';
import FisContainerColumn from 'fis-container-column';

export default class FisInputGroup extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        label: React.PropTypes.string,
        isInvalid: React.PropTypes.bool,
        infoflow: React.PropTypes.bool,
        children: React.PropTypes.oneOfType([React.PropTypes.arrayOf(React.PropTypes.element), React.PropTypes.element])
    };

    static defaultProps = {
        errorMessage: '',
        errorVisible: false
    };

    state = {
        errorMessage: null,
        errorPrevMessage: null,
        errorClass: null
    };

    focusError = (item) => {
        this.setState({
            errorMessage: item.errormessage,
            errorPrevMessage: item.errormessage,
            errorClass: 'visible'
        });
    };

    hideError = () => {
        this.setState({
            errorMessage: null,
            errorClass: null,
            errorPrevMessage: null
        });
    };

    mouseOverError = (item) => {
        this.setState({
            errorMessage: item.errormessage,
            errorClass: 'visible'
        });
    };

    mouseOutError = () => {
        if (this.state.errorPrevMessage) {
            this.setState({
                errorMessage: this.state.errorPrevMessage,
                errorClass: 'visible'
            });
        } else {
            this.setState({
                errorMessage: null,
                errorClass: null
            });
        }
    };

    extendChild(child) {
        return React.cloneElement(child, {
            focusError: this.focusError,
            hideError: this.hideError,
            mouseOverError: this.mouseOverError,
            mouseOutError: this.mouseOutError
        });
    }

    render() {
        const alertClasses = classnames('alert label', this.state.errorClass);

        const labelClasses = classnames('input-wrap', {
            'no-label': this.props.label === ' '
        });

        const renderChildren = React.Children.map(this.props.children, (child) => {
            return (
                <FisContainerColumn className={child.props.className} colSpan={child.props.colSpan}>
                    {this.extendChild(child)}
                </FisContainerColumn>
            );
        });

        return (
            <FisLabel
                label={this.props.label}
                isInvalid={this.props.isInvalid}
                infoflow={this.props.infoflow}
                className={labelClasses}
            >
                <div className="fis-input-group input-group">
                    <FisContainerRow>
                        { renderChildren }
                    </FisContainerRow>
                </div>
                <span
                    display-if={this.state.errorClass !== null}
                    className={alertClasses}
                >{this.state.errorMessage}</span>
            </FisLabel>
        );
    }
}
