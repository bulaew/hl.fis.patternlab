
module.exports = (context) => {
    return () => {
        return context.gulp.src([
            '/components/**/*.scss',
            '!css/00-globals/svgsprite-template.scss',
            '!css/00-globals/_svgsprite.scss'], { cwd: context.fisuicomponents.src })
            .pipe(context.gulpPlugins.cached('fis:ui:sass:lint'))
            .pipe(context.gulpPlugins.scssLint({ config: './_gulp-scsslint.yml' }))
            .pipe(context.gulpPlugins.scssLint.failReporter('E'))
            .on('error', context.gulpNotify.onError('<%= error.message %>'));
    };
};
