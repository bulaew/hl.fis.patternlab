
module.exports = (context) => {
    return () => {
        const postcssProcessors = require('../../postcss.config.js')();

        const SASS_OPTIONS = {
            outputStyle: 'expanded',
            precision: 8
        };

        return context.gulp.src('components/index.scss', { cwd: context.fisuicomponents.src })
            .pipe(context.gulpPlugins.rename('fisui.scss'))
            .pipe(context.gulpPlugins.sourcemaps.init())
            .pipe(context.gulpPlugins.sass(SASS_OPTIONS).on('error', context.gulpPlugins.sass.logError))
            .pipe(context.gulpPlugins.postcss(postcssProcessors))
            .pipe(context.gulpPlugins.sourcemaps.write('./'))
            .pipe(context.gulp.dest(`${context.fisuicomponents.target}/css`));
    };
};
