# Future-FIS README

### Styleguide

This is the living styleguide for FIS Web. The styleguide is build with [Patternlab](https://github.com/pattern-lab/patternlab-node).

The [styleguide](http://gitlab/fisweb/framework/tree/master/styleguide) contains the visual design of FIS Web.

### Framework
See the [Wiki](http://gitlab/fisweb/framework/wikis/home) for details.


### Quick Start

* Install NodeJs
* Change into the working directory
* Run `npm install` once 

### Browse the Styleguide
* Run npm install from the command line
* Run gulp or gulp serve from the command line


### READ the documentation and coding guidelines before start coding !!!
* [JS in generell](https://github.com/airbnb/javascript)
* [ReactJS](https://github.com/airbnb/javascript/tree/master/react)
* [LESS/SASS/CSS](https://github.com/airbnb/css)
* even [HTML](https://google.github.io/styleguide/htmlcssguide.xml#HTML_Style_Rules) 

Contrary to / in addition to the rules above we

* Use soft tabs (4 spaces) for indentation in CSS + SASS + JS + HTML
* write documentation above EVERY function and code of importance  in [JSdoc-Style](https://github.com/jsdoc3/jsdoc) 
* Use OOCSS Guidelines for implementation
  * a short text about OOCSS-Howto in SCSS is [here](http://thesassway.com/intermediate/using-object-oriented-css-with-sass)
  * Getting started working with OOCSS could take time. 
  * Avoid the descendent selector (i.e. don’t use .sidebar h3)
  * Avoid IDs as styling hooks
  * Avoid attaching classes to elements in your stylesheet (i.e. don’t do div.header or h1.title)
  * Except in some rare cases, avoid using !important
  * Use CSS Lint to check your CSS (and know that it has options and method to its madness)
  * Use CSS grids


If any rule you read isn't implemented in the config, please

* change it
* and make a seperate Commit with 'LINT CHANGE' in commit message

### Gulp
* please mark changes in Gulp with 'GULP CHANGE' in commit message
* please mark changes in READMEs with 'README CHANGE' in commit message

### JIRA Git-Integration
Read [this](http://doc.gitlab.com/ee/integration/jira.html)

### UI Iteration №2
https://drive.google.com/open?id=0BxyYHX2pbP0xcUhuWjEwczBYS2c


