import React from 'react';

import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisPalettePattern extends React.Component {
    render() {
        return (
            <div>
                <h1>Colors</h1>
                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2 className="demo-color-label">Primary colors</h2>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={100}>
                        <div className="demo-box">
                            <div className="color-box fis-bg-primary__base main">primary</div>
                            <div className="color-box fis-bg-secondary__base main">secondary</div>
                            <div className="color-box fis-bg-tertiary__base main">tertiary</div>
                        </div>
                    </FisContainerColumn>
                </FisContainerRow>
                <hr />
                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2>Additional colors </h2>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={100}>
                        <div className="demo-box">
                            <div className="color-box fis-bg-addon-1__base additional">addon-1</div>
                            <div className="color-box fis-bg-addon-2__base additional">addon-2</div>
                            <div className="color-box fis-bg-addon-3__base additional">addon-3</div>
                        </div>
                        <div className="demo-box">
                            <div className="color-box fis-bg-addon-4__base additional">addon-4</div>
                            <div className="color-box fis-bg-addon-5__base additional">addon-5</div>

                        </div>
                    </FisContainerColumn>
                </FisContainerRow>
                <hr />

                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2>Greyscale colors </h2>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={100}>
                        <div className="demo-box">
                            <div className="color-box fis-bg-grayscale-1__base additional">grayscale-1</div>
                            <div className="color-box fis-bg-grayscale-2__base additional">grayscale-2</div>
                            <div className="color-box fis-bg-grayscale-3__base additional">grayscale-3</div>
                            <div className="color-box fis-bg-grayscale-4__base additional">grayscale-4</div>
                            <div className="color-box fis-bg-grayscale-5__base additional">grayscale-5</div>
                        </div>
                    </FisContainerColumn>
                </FisContainerRow>

                <hr />

                <h2>Only use these colors for table-backgrounds</h2>
                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2>base pallete</h2>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={100}>
                        <div className="demo-box">
                            <div className="color-box fis-bg-ecellent__base additional">ecellent__base</div>
                            <div className="color-box fis-bg-very-good__base additional">very-good__base</div>
                            <div className="color-box fis-bg-good__base additional">good__base</div>
                            <div className="color-box fis-bg-average__base additional">average___base</div>
                            <div className="color-box fis-bg-below-average__base additional">below-average__base</div>
                        </div>
                        <div className="demo-box">
                            <div className="color-box fis-bg-bad__base additional">bad__base</div>
                            <div className="color-box fis-bg-very-bad__base additional">very-bad__base</div>
                            <div className="color-box fis-bg-remark-1__base additional">remark-1__base</div>
                            <div className="color-box fis-bg-remark-2__base additional">remark-2__base</div>
                            <div className="color-box fis-bg-not-relevant__base additional">not-relevant__base</div>
                        </div>
                    </FisContainerColumn>
                </FisContainerRow>

                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2>grey-style pallete</h2>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={100}>
                        <div className="demo-box">
                            <div className="color-box fis-bg-ecellent__grey-style additional">ecellent__grey-style</div>
                            <div className="color-box fis-bg-very-good__grey-style additional">very-good__grey-style</div>
                            <div className="color-box fis-bg-good__grey-style additional">good__grey-style</div>
                            <div className="color-box fis-bg-average__grey-style additional">average__grey-style</div>
                            <div className="color-box fis-bg-below-average__grey-style additional">below-average___grey-style</div>
                        </div>
                        <div className="demo-box">
                            <div className="color-box fis-bg-bad__grey-style additional">bad__grey-style</div>
                            <div className="color-box fis-bg-very-bad__grey-style additional">very-bad__grey-style</div>
                            <div className="color-box fis-bg-remark-1__grey-style additional">remark-1__grey-style</div>
                            <div className="color-box fis-bg-remark-2__grey-style additional">remark-2__grey-style</div>
                            <div className="color-box fis-bg-not-relevant__grey-style additional">not-relevant__grey-style</div>
                        </div>
                    </FisContainerColumn>
                </FisContainerRow>
            </div>
        );
    }
}
