import React from 'react';

import { FisInputBase } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisGridPattern extends React.Component {
    render() {
        const fisGridColspans = [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
            16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
            28, 29, 30, 100
        ];
        const rows = [];
        fisGridColspans.forEach((entry) => {
            const w = 'M'.repeat(entry);
            rows.push(
                <FisContainerRow key={entry} style={{ marginBottom: '10px !important' }}>
                    <FisContainerColumn colSpan={4}>{entry}:</FisContainerColumn>
                    <FisContainerColumn colSpan={entry}>
                         <FisInputBase placeholder="Name" defaultValue={w} />
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={1}>
                        <FisInputBase placeholder="Name" defaultValue={w} />
                    </FisContainerColumn>
                </FisContainerRow>
            );
        });


        return (
            <div>
                {rows}
            </div>
        );
    }
}
