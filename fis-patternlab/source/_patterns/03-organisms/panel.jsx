import React from 'react';

import { FisPanel } from 'fisui.js';
import { FisStripe } from 'fisui.js';

export default class FisPanelPattern extends React.Component {
    render() {
        const buttonsGroups = [
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'content-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'content-default', action: '#/save' },
                    { id: 'dl3', title: 'Excel', variant: 'content-default', action: '#/save' },
                    { id: 'dl4', title: 'Word', variant: 'content-default', action: '#/save' }
                ]
            },
            {
                buttons: [
                    { id: 'dl4', title: 'Mass Select', variant: 'content-default', action: '#/save', disabled: true },
                    { id: 'dl5', title: 'Clear', variant: 'content-default', action: '#/save' },
                    { id: 'dl6', title: 'Search', variant: 'content-hero', action: '#/save' }
                ]
            }
        ];

        const buttonsGroupsPanelTop = [
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'actionbar-destructive', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-destructive', action: '#/save' }

                ]
            }
        ];
        const buttonsGroupsPanelMiddle = [{
            buttons: [
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' }
            ]
        },
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' }
                ]
            },
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                ]
            },
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                ]
            }
        ];
        const buttonsGroupsPanelBottom = [
            {
                buttons: [
                    { id: 'dl4', title: 'Mass Select', variant: 'actionbar-destructive', action: '#/save', disabled: true },
                    { id: 'dl6', title: 'Search', variant: 'actionbar-constructive', action: '#/save' }
                ]
            }
        ];


        return (
            <FisPanel
                id="T9500"
                buttonGroupsTop={buttonsGroupsPanelTop}
                buttonGroupsMiddle={buttonsGroupsPanelMiddle}
                buttonGroupsBottom={buttonsGroupsPanelBottom}
            >
                <div>
                    <FisStripe
                        title="Search"
                        isOpened
                        buttonGroups={buttonsGroups}
                    >
                        <div>GANZ VIEL SUCHE<br /><br /><br /><br /><br /><br /><br />
                            <br /><br /><br /><br /><br /><br /><br /><br /><br />
                            <br /><br /><br /><br /><br /><br /><br /><br /><br />
                            <br /><br /><br /><br /><br /><br /><br /><br /><br />
                            <br /><br />
                            <br /><br /><br /><br /><br /><br /><br /><br /><br />
                            <br />
                            <br /><br /><br /><br /><br /><br /><br /><br /><br />
                            <br />
                            <br /><br /><br /><br /><br /><br /><br /><br />
                            <br /><br />
                            <br /><br /><br /><br /><br /><br /><br />
                            <br /><br /><br />
                            <br /><br /><br /><br /><br /><br />
                            <br /><br /><br /><br />

                        </div>
                    </FisStripe>

                    <FisStripe
                        title="Result"
                        isOpened={false}
                        buttonGroups={buttonsGroups}
                    >
                        <div>GANZ VIEL RESULTS</div>
                    </FisStripe>

                    <FisStripe
                        title="Detail"
                        isOpened={false}
                        buttonGroups={buttonsGroups}
                    >
                        <div>GANZ VIEL DETAIL</div>
                    </FisStripe>

                </div>
            </FisPanel>
        );
    }
}
