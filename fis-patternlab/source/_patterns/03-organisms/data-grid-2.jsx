import React from 'react';

import { FisDataGrid2 } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisDataGridPattern2 extends React.Component {

    render() {
        const data = [
            {
                'index': 1,
                'id': 'id_0',
                'avartar': 'https://s3.amazonaws.com/uifaces/faces/twitter/VMilescu/128.jpg',
                'city': 'East Rosalynville',
                'country': 'USA',
                'email': 'Garnett97@yahoo.com',
                'firstName': 'Leslie',
                'lastName': 'Conn',
                'street': 'Silas Lock',
                'zipCode': '27601-7334',
                'date': '2014-11-24T21:43:33.081Z',
                'bs': 'value-added empower bandwidth',
                'grade': 4,
                'catchPhrase': 'Balanced methodical artificial intelligence',
                'companyName': 'Pfannerstill and Sons',
                'words': [
                    'nihil',
                    'vitae',
                    'enim'
                ],
                'sentence': 'quia tenetur et hic eveniet ad consectetur nesciunt commodi'
            },
            {
                'index': 2,
                'id': 'id_1',
                'avartar': 'https://s3.amazonaws.com/uifaces/faces/twitter/increase/128.jpg',
                'city': 'West Edward',
                'country': 'USA',
                'email': 'Valerie_Bailey93@hotmail.com',
                'firstName': 'Tyrel',
                'lastName': 'Veum',
                'street': 'Minnie Ridges',
                'zipCode': '84828',
                'date': '2014-05-26T21:40:33.023Z',
                'bs': 'next-generation enable platforms',
                'grade': 7,
                'catchPhrase': 'Upgradable real-time attitude',
                'companyName': 'Champlin, Hagenes and Reinger',
                'words': [
                    'fuga',
                    'vitae',
                    'recusandae'
                ],
                'sentence': 'neque dicta tenetur cum autem'
            },
            {
                'index': 3,
                'id': 'id_2',
                'avartar': 'https://s3.amazonaws.com/uifaces/faces/twitter/plasticine/128.jpg',
                'city': 'Brakusfurt',
                'country': 'Spain',
                'email': 'Marietta_Rowe50@yahoo.com',
                'firstName': 'Ansel',
                'lastName': 'Lynch',
                'street': 'Huel Stream',
                'zipCode': '60013-3921',
                'date': '2015-02-16T10:11:18.668Z',
                'bs': 'holistic revolutionize solutions',
                'grade': 3,
                'catchPhrase': 'Quality-focused mission-critical framework',
                'companyName': 'Senger-Smith',
                'words': [
                    'voluptatem',
                    'optio',
                    'ipsu m'
                ],
                'sentence': 'molestias eligendi odio ut illum voluptatem a autem'
            },
            {
                'index': 4,
                'id': 'id_3',
                'avartar': 'https://s3.amazonaws.com/uifaces/faces/twitter/panghal0/128.jpg',
                'city': 'Lake Maximilliafort',
                'country': 'USA',
                'email': 'Ashlee24@yahoo.com',
                'firstName': 'Allan',
                'lastName': 'Gutmann',
                'street': 'Sanford Glens',
                'zipCode': '44409',
                'date': '2014-06-02T02:22:50.237Z',
                'bs': 'user-centric unleash models',
                'grade': 4,
                'catchPhrase': 'Future-proofed zero defect initiative',
                'companyName': 'Rau, Becker and Nicolas',
                'words': [
                    'praesentium',
                    'dolores',
                    'odit'
                ],
                'sentence': 'dolore dolorem nihil mollitia'
            },
            {
                'index': 5,
                'id': 'id_0',
                'avartar': 'https://s3.amazonaws.com/uifaces/faces/twitter/VMilescu/128.jpg',
                'city': 'East Rosalynville',
                'country': 'USA',
                'email': 'Garnett97@yahoo.com',
                'firstName': 'Leslie',
                'lastName': 'Conn',
                'street': 'Silas Lock',
                'zipCode': '27601-7334',
                'date': '2014-11-24T21:43:33.081Z',
                'bs': 'value-added empower bandwidth',
                'grade': 4,
                'catchPhrase': 'Balanced methodical artificial intelligence',
                'companyName': 'Pfannerstill and Sons',
                'words': [
                    'nihil',
                    'vitae',
                    'enim'
                ],
                'sentence': 'quia tenetur et hic eveniet ad consectetur nesciunt commodi'
            },
            {
                'index': 6,
                'id': 'id_1',
                'avartar': 'https://s3.amazonaws.com/uifaces/faces/twitter/increase/128.jpg',
                'city': 'West Edward',
                'country': 'USA',
                'email': 'Valerie_Bailey93@hotmail.com',
                'firstName': 'Tyrel',
                'lastName': 'Veum',
                'street': 'Minnie Ridges',
                'zipCode': '84828',
                'date': '2014-05-26T21:40:33.023Z',
                'bs': 'next-generation enable platforms',
                'grade': 7,
                'catchPhrase': 'Upgradable real-time attitude',
                'companyName': 'Champlin, Hagenes and Reinger',
                'words': [
                    'fuga',
                    'vitae',
                    'recusandae'
                ],
                'sentence': 'neque dicta tenetur cum autem'
            },
            {
                'index': 7,
                'id': 'id_2',
                'avartar': 'https://s3.amazonaws.com/uifaces/faces/twitter/plasticine/128.jpg',
                'city': 'Brakusfurt',
                'country': 'Spain',
                'email': 'Marietta_Rowe50@yahoo.com',
                'firstName': 'Ansel',
                'lastName': 'Lynch',
                'street': 'Huel Stream',
                'zipCode': '60013-3921',
                'date': '2015-02-16T10:11:18.668Z',
                'bs': 'holistic revolutionize solutions',
                'grade': 3,
                'catchPhrase': 'Quality-focused mission-critical framework',
                'companyName': 'Senger-Smith',
                'words': [
                    'voluptatem',
                    'optio',
                    'ipsum'
                ],
                'sentence': 'molestias eligendi odio ut illum voluptatem a autem'
            },
            {
                'index': 8,
                'id': 'id_3',
                'avartar': 'https://s3.amazonaws.com/uifaces/faces/twitter/panghal0/128.jpg',
                'city': 'Lake Maximilliafort',
                'country': 'USA',
                'email': 'Ashlee24@yahoo.com',
                'firstName': 'Allan',
                'lastName': 'Gutmann',
                'street': 'Sanford Glens',
                'zipCode': '44409',
                'date': '2014-06-02T02:22:50.237Z',
                'bs': 'user-centric unleash models',
                'grade': 4,
                'catchPhrase': 'Future-proofed zero defect initiative',
                'companyName': 'Rau, Becker and Nicolas',
                'words': [
                    'praesentium',
                    'dolores',
                    'odit'
                ],
                'sentence': 'dolore dolorem nihil mollitia'
            }
        ];


        const columns = [
            {
                headerName: 'Athlete Details',
                children: [
                    { headerName: 'Firstame', field: 'firstName', width: 120 },
                    { headerName: 'Lastname', field: 'lastName', width: 110 },
                    { headerName: 'eMail', field: 'email', width: 150 },
                    { headerName: 'Grade', field: 'grade', width: 80, minWidth: 50 },
                    { headerName: 'Country', field: 'country', width: 180 }
                ]
            },
            {
                headerName: 'Sports Results',
                children: [
                    { headerName: 'catchPhrase', field: 'catchPhrase' },
                    { headerName: 'companyName', field: 'companyName' },
                    { headerName: 'sentence', field: 'words' }
                ]
            }
        ];

        return (
            <div>
                <h1>Data Grid</h1>
                <form>
                    <FisContainerRow >
                        <FisContainerColumn colSpan={130}>
                            <FisDataGrid2
                                rowData={data}
                                columnDefs={columns}
                            />
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
