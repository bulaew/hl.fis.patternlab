import React from 'react';
import { FisTabPanel } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

// FIXME: DELETE WHEN  IS REALLY USED
/* eslint-disable */
export default class FisTabPanelPattern extends React.Component {
    render() {
        const tabData = [
            {
                title: 'Routing',
                id: '1',
                content: (
                    <div>
                        Lorem <a href="#">ipsum</a> dolor sit amet
                    </div>
                ),
            },
            {
                title: 'Shipment',
                id: '2',
                content: (
                    <div>
                        Ut <a href="#">enim</a> ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.
                    </div>
                ),
            },
            {
                title: 'Cargo/Container',
                id: '3',
                content: (
                    <div>
                        Duis <a href="#">aute</a> irure dolor in reprehenderit in voluptate
                        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                        sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                        mollit anim id est laborum.
                    </div>
                ),
            },
        ];

        return (
            <div>
                <h1>Tabs</h1>
                <FisContainerRow style={{ marginBottom: 20 }}>
                    <FisContainerColumn colSpan={40}>
                        <FisTabPanel tabData={tabData} />
                    </FisContainerColumn>
                </FisContainerRow>

                <FisContainerRow >
                    <FisContainerColumn colSpan={47}>
                        <FisTabPanel tabData={tabData} />
                    </FisContainerColumn>
                </FisContainerRow>
            </div>

        );
    }
}
