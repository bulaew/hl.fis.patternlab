import React from 'react';

import { FisClientHeader } from 'fisui.js';

export default class FisClientHeaderPattern extends React.Component {
    render() {
        return (
            <FisClientHeader />
        );
    }
}
