import React from 'react';

import { FisButtonGroup } from 'fisui.js';
import { FisButton } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisButtonGroupPattern extends React.Component {
    render() {
        return (
            <div>
                <h1>Button Group</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong> default</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={60} className="demo-col">
                            <FisButtonGroup>
                                <FisButton tag="a" variant="content-default" href="#">Primary</FisButton>
                                <FisButton tag="a" variant="content-default" href="#">Secondary</FisButton>
                                <FisButton tag="a" variant="content-hero" href="#">Tertiary</FisButton>
                            </FisButtonGroup>
                            <FisButtonGroup>
                                <FisButton variant="content-default">Primary</FisButton>
                                <FisButton variant="content-default">Secondary</FisButton>
                                <FisButton variant="content-hero">Tertiary</FisButton>
                            </FisButtonGroup>
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
