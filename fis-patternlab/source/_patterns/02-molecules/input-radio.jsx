import React from 'react';

import { FisCheckboxRadioGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisInputRadioPattern extends React.Component {
    render() {
        const inputlist = [
            { name: 'Name', checked: true, value: '1', label: 'Name', id: 't1' },
            { name: 'TmpSep', checked: false, value: '2', label: 'TmpSep', id: 't2' },
            { name: 'TranEx', checked: false, value: '3', label: 'TranEx', id: 't3' },
            { name: 'ShTy', checked: false, value: '4', label: 'ShTy', id: 't4' }
        ];

        const optsDefault = {
            name: 'myradio1',
            label: 'Your Radios 1',
            disabled: false,
            mandatory: false,
            type: 'radio'
        };

        const optsDisabled = {
            name: 'myradio2',
            label: 'Your Radios 2',
            disabled: true,
            mandatory: false,
            type: 'radio'
        };

        const optsError = {
            name: 'myradio3',
            label: 'Your Radios 3',
            isInvalid: true,
            errorMessage: 'A very long Error message',
            disabled: false,
            mandatory: false,
            type: 'radio'
        };

        const optsMandatory = {
            name: 'myradio4',
            label: 'Your Radios 4',
            disabled: false,
            mandatory: true,
            stacked: true,
            type: 'radio'
        };
        const optsInfoFlow = {
            name: 'myradio4',
            label: 'Your Radios 4',
            disabled: false,
            mandatory: true,
            infoflow: true,
            stacked: true,
            type: 'radio'
        };

        return (
            <div>
                <h1>Inputs[type='radio']</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>default</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDefault} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDefault} />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>disabled</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDisabled} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDisabled} />
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>error</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsError} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsError} />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>mandatory</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsMandatory} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsMandatory} />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>mandatory, stacked with Info-Flow</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsInfoFlow} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsInfoFlow} />
                        </FisContainerColumn>
                    </FisContainerRow>


                </form>
            </div>
        );
    }
}
