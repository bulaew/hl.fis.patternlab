import React from 'react';

import { FisFieldset } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisButtonIcon } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisLabel } from 'fisui.js';

export default class FisFieldsetPattern extends React.Component {
    render() {
        return (
          <div>
              <h1>Fieldset</h1>
              <FisContainerRow>
                  <FisContainerColumn colSpan={10}>
                      <h2>
                          <span className="info">state: <strong>default</strong></span>
                      </h2>
                  </FisContainerColumn>
                  <FisContainerColumn colSpan={20} className="">
                      <FisFieldset legend="Transportation request" infoflow>
                          <FisContainerRow>
                              <FisContainerColumn colSpan={20}>
                                  <FisLabel label="Resp. TD Office" >
                                      <FisContainerRow>
                                          <FisContainerColumn colSpan={8}>
                                              <FisInputBase placeholder="Name" defaultValue="feld0" />
                                          </FisContainerColumn>
                                          <FisContainerColumn colSpan={8}>
                                              <FisInputBase placeholder="Name" defaultValue="feld 1" />
                                          </FisContainerColumn>
                                          <FisContainerColumn colSpan={4}>
                                              <FisButtonIcon variant="default" tag="a" icon="section-open" />
                                          </FisContainerColumn>
                                      </FisContainerRow>
                                  </FisLabel>
                              </FisContainerColumn>
                          </FisContainerRow>
                      </FisFieldset>
                  </FisContainerColumn>
              </FisContainerRow>
          </div>
      );
    }
}
