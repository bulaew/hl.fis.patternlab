
module.exports = (context) => {
    return () => {
        // configuration for various plugins
        const SASS_OPTIONS = {
            outputStyle: 'expanded',
            precision: 8
        };

        return context.gulp.src('source/_styleguide/css/*.scss', { cwd: context.fispatternlab.src })
            .pipe(context.gulpPlugins.sass(SASS_OPTIONS).on('error', context.gulpPlugins.sass.logError))
            .pipe(context.gulp.dest('public/styleguide/css', { cwd: context.fispatternlab.src }));
    };
};
