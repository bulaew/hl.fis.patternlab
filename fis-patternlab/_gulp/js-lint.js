module.exports = (context) => {
    return () => {
        const dir = context.fispatternlab.src;
        return context.gulp.src([`${dir}/source/_patterns/**/*.js`, `${dir}/source/_patterns/**/*.jsx`])
            .pipe(context.gulpPlugins.cached('lab:js:lint'))
            .pipe(context.gulpPlugins.eslint())
            .pipe(context.gulpPlugins.eslint.format())
            .pipe(context.gulpPlugins.if(!context.browserSync.active, context.gulpPlugins.eslint.failAfterError()));
    };
};
