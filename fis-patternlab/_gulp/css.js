
module.exports = (context) => {
    return () => {
        const postcssProcessors = require('../../postcss.config.js')();

        const SASS_OPTIONS = {
            outputStyle: 'expanded',
            precision: 8
        };

        return context.gulp.src('source/_patterns/patterns.scss', { cwd: context.fispatternlab.src })
            .pipe(context.gulpPlugins.sourcemaps.init())
            .pipe(context.gulpPlugins.sass(SASS_OPTIONS).on('error', context.gulpPlugins.sass.logError))
            .pipe(context.gulpPlugins.postcss(postcssProcessors))
            .pipe(context.gulpPlugins.sourcemaps.write('./'))
            .pipe(context.gulp.dest('public/patterns', { cwd: context.fispatternlab.src }))
            .pipe(context.gulpPlugins.if(context.browserSync.active, context.browserSync.stream({ match: '**/*.css' })));
    };
};
